#!/bin/bash

gpioArray=( 14 2 15 3 18 4 23 17 24 27 25 22 8 10 7 9 )
gpioArray=( 14 2 15 3 18 17 23 27 24 22 25 10 8 9 7 11 )
gpioArray=( 15 3 18 17 23 27 24 22 25 10 )
delay=.5

for i in "${gpioArray[@]}"
do
	echo $i > /sys/class/gpio/export
	echo "out" > "/sys/class/gpio/gpio$i/direction"
done

for i in "${gpioArray[@]}"
do
	echo "0" > "/sys/class/gpio/gpio$i/value"
done

sleep 1

for j in {1..2}
do

	for i in "${gpioArray[@]}"
	do
		echo "Enabling $i"
		echo "1" > "/sys/class/gpio/gpio$i/value"
		sleep $delay
		echo "0" > "/sys/class/gpio/gpio$i/value"
	done
done


for i in "${gpioArray[@]}"
do
	echo $i > /sys/class/gpio/unexport
done


