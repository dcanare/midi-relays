#!/bin/bash

src=/home/pi/midi-relays/bin
dest=/usr/bin

binIt () {
	if [ -z "$2" ]; then
		b=$1
	else
		b=$2
	fi

	echo "Installing $dest/$b..."
	sudo mv $dest/$b /tmp/ 2>/dev/null
	sudo ln -s $src/$1 $dest/$b
}

binIt skip-track
binIt kill-playlist
binIt run-playlist
binIt send-shutdown
binIt send-reboot

binIt affect-channel.py affect-channel
binIt relay-controller.py relay-controller
binIt watchdog.sh watchdog

binIt force-analog-audio.sh force-analog-audio
binIt phone-home.sh phone-home

echo "Done."

