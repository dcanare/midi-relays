#!/bin/bash

if [ -z "$1" ]; then
	echo "Executes a command repeatedly (in case it dies)"
	echo ""
	echo "Usage: $0 command"
	exit 1
fi


while [ 1 ]; do
	$*
	sleep 2
done
