#!/usr/bin/python

import serial
import time

ser = serial.Serial('/dev/ttyUSB0', 115200)

def toByte(pin, state):
	return chr(pin*2 + state)

for i in range(16):
	ser.write(toByte(i, 1))
	ser.write(toByte(i, 0))
	ser.write(toByte(i, 1))
	ser.write(toByte(i, 0))

time.sleep(1)

for i in range(16):
	ser.write(toByte(i, i%2))
	time.sleep(0.25)
	
for i in range(16):
	ser.write(toByte(i, 1-(i%2)))
	time.sleep(0.25)

for i in range(16):
	ser.write(toByte(i, 0))

time.sleep(2)
for i in range(16):
	ser.write(toByte(i, 1))
	time.sleep(0.25)

for i in range(16):
	ser.write(toByte(i, 0))
	time.sleep(0.25)

