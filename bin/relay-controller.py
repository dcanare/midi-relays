#!/usr/bin/python

import pyalsa, sys
from pyalsa import alsaseq
from pyalsa.alsaseq import *
import serial

ser = serial.Serial('/dev/ttyUSB0', 115200)

def output(data):
	ser.write(data)

def toByte(pin, state):
	return chr(pin*2 + state)

def processEvent(event):
	type = event.type
	data = event.get_data()
	if type == SEQ_EVENT_NOTEOFF or (type == SEQ_EVENT_NOTEON and data['note.velocity'] == 0):
		output(toByte(data['note.channel'], 0))
	elif type == SEQ_EVENT_NOTEON:
		output(toByte(data['note.channel'], 1))

sequencer = Sequencer(name="default", clientname="GreenLightGo MIDI Relays", streams=alsaseq.SEQ_OPEN_DUPLEX, mode=alsaseq.SEQ_NONBLOCK)
port = sequencer.create_simple_port(
	name="MIDI Input 0",
	type=alsaseq.SEQ_PORT_TYPE_MIDI_GENERIC|alsaseq.SEQ_PORT_TYPE_APPLICATION,
	caps=alsaseq.SEQ_PORT_CAP_WRITE|alsaseq.SEQ_PORT_CAP_SUBS_WRITE
)

while True:
	eventlist = sequencer.receive_events(timeout=180000, maxevents=1)
	for event in eventlist:
		processEvent(event)

