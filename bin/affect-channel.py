#!/usr/bin/python

import sys
import pyalsa
from pyalsa import alsaseq
from pyalsa.alsaseq import *

# argv[1] = channel # (-1 for all 16)
# argv[2] = 0 or 1 (off or on)
# argv[3] = output port
# argv[4] = output port

def makeEvent(channel, on, destination):
	if on:
		event = alsaseq.SeqEvent(alsaseq.SEQ_EVENT_NOTEON)
		event.set_data({'note.channel' : channel, 'note.note' : channel+1, 'note.velocity' : 127})
	else:
		event = alsaseq.SeqEvent(alsaseq.SEQ_EVENT_NOTEOFF)
		event.set_data({'note.channel' : channel, 'note.note' : channel+1, 'note.velocity' : 0})

	event.dest = destination

	return event

seq = alsaseq.Sequencer()
event = alsaseq.SeqEvent(alsaseq.SEQ_EVENT_NOTEON)

channel = int(sys.argv[1])
on = (sys.argv[2] == '1')
dest = (int(sys.argv[3]), int(sys.argv[4]))

if channel == -1:
	for i in range(16):
		seq.output_event(makeEvent(i, on, dest))
else:
	seq.output_event(makeEvent(channel, on, dest))

seq.drain_output()
