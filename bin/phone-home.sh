#!/bin/bash

sed -i.bak '/disabled=1/d' /etc/wpa_supplicant/wpa_supplicant.conf

affect-channel 0 1 128 0
sleep 0.5
affect-channel 0 0 128 0
sleep 0.5
affect-channel 0 1 128 0
sleep 0.5
affect-channel 0 0 128 0

ping -c1 domstyle.net
status=$?
while [ $status -ne 0 ]; do
    /etc/init.d/networking restart
    ifdown wlan0
    ifup wlan0
    for i in `seq 1 10`; do
	ping -c1 domstyle.net
	status=$?
	if [ $status -ne 0 ]; then
	    sleep 3
	else
	    break
	fi
    done
done

ipAddr=`hostname -I`
host=`hostname`
message="MIDI-Relay:$host:$ipAddr"
wget -O /tmp/ping "domstyle.net/ping.php?message=$message&travis=1"

for i in `seq 0 15`; do
	affect-channel $i 1 128 0
	sleep 0.1
	affect-channel $i 0 128 0
done

