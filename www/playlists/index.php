<?php
	include_once("../include/BasicTemplate.php");
	require_once("../include/Playlist.php");
	
	$template = new BasicTemplate(
		file_get_contents("../template.html"),
		'Playlists'
	);
	$template->bufferStart();

	$playlistNames = Playlist::getPlaylists();
?>

					<h2>Playlists</h2>
					<ul>
<?php
	if(empty($playlistNames)){
		echo '
						<li>You have no playlists!</li>';
	}else{
		foreach($playlistNames as $playlist){
			echo "
						<li><a href='?list=$playlist'>$playlist</a></li>";
		}
	}
?>
					</ul>

<?php
	if(!empty($_GET['list'])){
		echo "
					<h2>$_GET[list]</h2>
					<ol>";

		$playlist = Playlist::load($_GET['list']);
		foreach($playlist->shows as $song){
			echo "
						<li>$song</li>";
		}
		echo "
					</ol>";
	}
?>


<?php $template->bufferStop('PAGE_CONTENT'); ?>
