<?php
require_once("include/Config.php");
require_once("include/Status.php");
require_once("include/BasicTemplate.php");

$template = new BasicTemplate(file_get_contents("template.html"), $STATUS->state);
$template->bufferStart();

echo $STATUS->getStatusHTML();

$template->bufferStop('PAGE_CONTENT');
