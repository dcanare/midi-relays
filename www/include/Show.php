<?php

require_once('Config.php');

class Show {
	static $list = [];
	
	static function getShows(){
		if(empty(Show::$list)){
			$fileList = scandir(Config::$baseDir . "shows/");
			$fileList = array_diff($fileList, array('.', '..'));

			foreach($fileList as $fileName){
				$pathParts = pathinfo($fileName);
				$base = $pathParts['filename'];
				if($base == "") continue;
				
				if(in_array("$base.mid", $fileList) && in_array("$base.wav", $fileList)){
					if(!in_array($base, Show::$list)){
						Show::$list[] = $base;
					}
				}
			}
		}
		
		return Show::$list;
	}
}

