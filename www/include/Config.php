<?php

session_start();
mt_srand();


class UserException extends Exception {}

class Config {
	public static $baseDir = '/opt/midi-relays/';
}

function requireLogin(){
	if(empty($_SESSION['authenticated'])){
		$_SESSION['redirect'] = $_SERVER['PHP_SELF'];
		header('Location: /login.php');
		exit();
	}
}
