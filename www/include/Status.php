<?php

require_once('Config.php');
require_once('Playlist.php');

class Status {
	function __construct() {
		$status = file(Config::$baseDir . 'status', FILE_IGNORE_NEW_LINES);
		if(empty($status)){
			$status = ['stopped'];
		}elseif(!is_array($status)){
			$status = [$status];
		}

		$this->state = ucfirst($status[0]);
		$this->playlistName = @$status[1];
		$this->trackNumber = @$status[2];
	}

	function setStatus($state, $playlist='', $trackNumber=-1){
		$this->state = $state;
		$this->playlist = $playlist;
		$this->trackNumber = $trackNumber;

		file_put_contents(Config::$baseDir.'status', $this->state . "\n" . $this->playlist . "\n" . $this->trackNumber . "\n");
	}

	function getStatusHTML(){
		$html = '
		
						<h2>Status: ' . $this->state . '</h2>';
		if(!empty($this->playlistName)){
			$html .= '
						<h2>Playlist: ' . $this->playlistName . '</h2>
						<ol>';

			$playlist = Playlist::load($this->playlistName);
			foreach($playlist->shows as $i=>$show){
				$class = ($i == $this->trackNumber) ? ' class="currentSong"' : '';
				
				$html .= "
							<li$class>$show</li>";
			}

			$html .= '
						</ol>';
		}

		return $html;
	}
}

$STATUS = new Status();
