<?php

require_once('Config.php');

class Playlist {
	static $list = [];
	
	function __construct($filename) {
		$this->shows = file($filename, FILE_IGNORE_NEW_LINES|FILE_SKIP_EMPTY_LINES);
	}

	static function load($name){
		return new Playlist(Config::$baseDir . "playlists/$name");
	}

	static function getPlaylists(){
		if(empty(Playlist::$list)){
			Playlist::$list = scandir(Config::$baseDir . "playlists/");
			
			if(empty(Playlist::$list)){
				Playlist::$list = [];
			}
			Playlist::$list = array_diff(Playlist::$list, array('.', '..'));
		}
		
		return Playlist::$list;
	}
}

