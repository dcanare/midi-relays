<?php
	require_once("../include/Config.php");
	require_once("../include/Status.php");
	require_once("../include/Playlist.php");

	requireLogin();

	function startPlaylist($playlist){
		exec('run-playlist "' . Config::$baseDir . '" ' . $playlist . ' > /tmp/playlist-output 2>&1 &');
	}

	function stopTrack(){
		exec('skip-track');
		allOff();
	}

	function stopPlaylist(){
		exec('kill-playlist');
		file_put_contents(Config::$baseDir . 'status', "stopped\n");
		allOff();
	}

	function allOff(){
		affectChannel(-1, 0);
	}

	function shutdown(){
		stopPlaylist();
		exec('send-shutdown');
	}

	function reboot(){
		stopPlaylist();
		exec('send-reboot');
	}

	function affectChannel($id, $state){
		if(empty($state)){
			$state = 0;
		}else{
			$state = 1;
		}
		exec("affect-channel $id $state 128 0");
	}

	if(!empty($_REQUEST['action'])){
		if($_REQUEST['action'] == 'SWITCH'){
			affectChannel($_REQUEST['light'], $_REQUEST['power']);
			exit();
		}else{
			if($_REQUEST['action'] == 'STOP'){
				stopPlaylist();
			}elseif($_REQUEST['action'] == 'CHANGE'){
				stopPlaylist();
				startPlaylist($_REQUEST['playlist']);
			}elseif($_REQUEST['action'] == 'SKIP'){
				stopTrack();
			}elseif($_REQUEST['action'] == 'SHUTDOWN'){
				shutdown();
			}elseif($_REQUEST['action'] == 'REBOOT'){
				reboot();
			}
			sleep(3);
		}
	}

	include_once("../include/BasicTemplate.php");
	$template = new BasicTemplate(file_get_contents("../template.html"), $STATUS->state);
	$template->bufferStart();


	$playlistNames = Playlist::getPlaylists();
?>

					<script type="text/javascript">
						function relMouseCoords(event){
							var offset = [0, 0];
							var elementPos = [0, 0];
							var currentElement = event.target;

							do{
								offset[0] += currentElement.offsetLeft - currentElement.scrollLeft;
								offset[1] += currentElement.offsetTop - currentElement.scrollTop;
							}
							while(currentElement = currentElement.offsetParent)

							elementPos[0] = event.pageX - offset[0];
							elementPos[1] = event.pageY - offset[1];

							return elementPos;
						}
						
						function powerButton(id){
							var event = window.event;

							var coords = relMouseCoords(event);
							var url = '?action=SWITCH&light=' + id + '&power=';
							if(coords[0] < coords[1]){
								url += '0';
								event.target.style.background = "#a70000";
							}else{
								url += '1';
								event.target.style.background = "#74a607";
							}

							var httpRequest;
							if (window.XMLHttpRequest) {
								httpRequest = new XMLHttpRequest();
							} else if (window.ActiveXObject) {
								httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
							}
							httpRequest.onreadystatechange = function(){
								if (httpRequest.readyState === 4) {
									event.target.style.background = "";
								}
							};
							httpRequest.open('GET', url, true);
							httpRequest.send(null)
						}
					</script>

					<h1>Playlist Control</h1>
					<form method="post">
						<input type="hidden" name="action" value="CHANGE" />
						<input style="float:right" type="submit" name="action" value="SKIP" />
						<input type="submit" name="action" value="STOP" />
						<hr/>
					
						<h2>
							Change Playlist:
							<select name="playlist" onchange="this.form.submit();">
								<option></option>
<?php
	foreach($playlistNames as $playlist){
		echo "
								<option value='$playlist'>$playlist</option>";
	}
?>
							</select>
						</h2>
					</form>
					<hr/>

<?php
	echo $STATUS->getStatusHTML();

	echo '
					<hr/>
					<h1>Manual Controls</h1>';
	for($i=0; $i<16; $i++){
		echo '
					<div class="powerButton" onclick="powerButton('.$i.');">' . ($i+1)  . '</div>';
	}
	echo '
					<hr style="clear: both;"/>
					<div class="powerButton" onclick="powerButton(-1);">ALL</div>
					<hr style="clear: both;"/>';
?>
					<h1>System Controls</h1>
					<form method="post">
						<input style="float:right" type="submit" name="action" value="REBOOT" />
						<input type="submit" name="action" value="SHUTDOWN" />
					</form>

<?php
	$template->bufferStop('PAGE_CONTENT');
