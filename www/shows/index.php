<?php
	include_once("../include/BasicTemplate.php");
	require_once("../include/Show.php");

	if(!empty($_POST)){
		if(empty($_POST['name'])){
			$pathParts = pathinfo($_FILES['audio']['name']);
			$showName = $pathParts['filename'];
		}else{
			$showName = $_POST['name'];
		}
		
		$audioOutputFile = Config::$baseDir . "shows/$showName.wav";
		$sequenceOutputFile = Config::$baseDir . "shows/$showName.mid";

		$pathParts = pathinfo($_FILES['audio']['name']);
		if($pathParts['extension'] == 'wav'){
			copy($_FILES['audio']['tmp_name'], $audioOutputFile);
		}else{
			$cmd = "sox --magic \"" . $_FILES['audio']['tmp_name'] . "\" -b16 -c1 -r22050 \"$audioOutputFile\"";
			exec($cmd);
		}
		copy($_FILES['sequence']['tmp_name'], $sequenceOutputFile);

		$showNames = Show::getShows();
		file_put_contents(Config::$baseDir . 'playlists/All_Shows', implode("\n", $showNames));
		
		header("Location: $_SERVER[PHP_SELF]");
		exit();

	}
	
	$template = new BasicTemplate(
		file_get_contents("../template.html"),
		'Shows'
	);
	$template->bufferStart();

	$showNames = Show::getShows();
?>

					<h2>Add a Show</h2>
					<form method="post" enctype="multipart/form-data">
						<table>
							<tr>
								<td>Show name:</td>
								<td><input type="text" name="name" /></td>
							</tr>
							<tr>
								<td>Audio file:</td>
								<td><input type="file" name="audio" accept="audio/mpeg,audio/x-wav"/></td>
							</tr>
							<tr>
								<td>Sequence file:</td>
								<td><input type="file" name="sequence" accept="audio/midi"/></td>
							</tr>
							<tr>
								<td colspan="2" style="text-align: center">
									<input type="submit" />
								</td>
							</tr>
						</table>
					</form>

					<hr/>
					<h2>Existing Shows</h2>
					<ul>
<?php
	if(empty($showNames)){
		echo '
						<li>You have no shows!</li>';
	}else{
		foreach($showNames as $show){
			echo "
						<li>$show</li>";
		}
	}
?>
					</ul>



<?php $template->bufferStop('PAGE_CONTENT'); ?>
