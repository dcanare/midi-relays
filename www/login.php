<?php
	require_once("include/Config.php");

	if(!empty($GET['action']) && $_GET['action'] == 'logout'){
			session_unset();
			header("Location: /");
			exit();
	}else{
		if(!empty($_SESSION['authenticated']) || hash('sha512', @$_POST['password']) == '573635ad03181e58944202cd2392aaa63f9418aba5f849ed719bd998d965ceffc3c10070c3f0c2d6fbed84a0307ad6182e1a60cb8492b3b6c3d608bbfb7ba05b'){
			$_SESSION['authenticated'] = true;
			if(!empty($_SESSION['redirect'])){
				header("Location: $_SESSION[redirect]");
				unset($_SESSION['redirect']);
				exit();
			}else{
				header('Location: /');
				exit();
			}
		}
	}

	include_once("include/BasicTemplate.php");

	$template = new BasicTemplate(file_get_contents("template.html"), 'Login');
	$template->bufferStart();

	if(!empty($_POST)){
		echo '

					<div class="error">Authentication failed.</div>';
	}
?>


					<form method="post">
						<table>
							<tr>
								<td>Password:</td>
								<td><input type="password" name="password" /></td>
							</tr>
							<tr>
								<td colspan="2"><input type="submit" /></td>
							</tr>
						</table>
					</form>
					
<?php $template->bufferStop('PAGE_CONTENT'); ?>
