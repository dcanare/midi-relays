

snd_seq_t* open_client();

int main(int argc, const char* argv[]){

	return 0;
}

// create a new client
snd_seq_t *open_client(){
        snd_seq_t *handle;
        int err;
        err = snd_seq_open(&handle, "default", SND_SEQ_OPEN_INPUT, 0);
        if (err < 0)
                return NULL;
        snd_seq_set_client_name(handle, "My Client");
        return handle;
}
