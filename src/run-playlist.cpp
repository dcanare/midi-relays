#include <string>
#include <iostream>
#include <fstream>
#include <list>

#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>

using namespace std;

//#define audioCMD string("sleep 0.3 && pifm ")
#define audioCMD string("sleep 0 && aplay ")
#define midiCMD string("aplaymidi -p 128:0 ")

void updateStatusFile(string filename, string status, string playlist, int track);

/**
 * argv[1] = path to base dir
 * argv[2] = name of playlist
 * argv[3] = (optional=3000) delay between songs (in ms)
 **/
int main(int argc, const char* argv[]){
	if(argc < 3){
		cout << "Usage: " << argv[0] << " path/to/baseDir/ nameOfPlaylist [delayBetweenSongs]" << endl;
		return 1;
	}
	setuid(0);

	string baseDir = argv[1];
	string playlist = argv[2];
	string outputFile = baseDir + "status";
	
	long delay = 3000;
	if(argc > 3){
		delay = atol(argv[3]);
	}

	// Load playlist
	updateStatusFile(outputFile, "loading", playlist, -1);
	string playlistFilename = baseDir + "playlists/" + playlist;
	cout << "Loading " << playlistFilename << endl;
	ifstream playlistFile(playlistFilename.c_str());
	list<string> shows;
	
	string line;
	for(int i=0; std::getline(playlistFile, line); i++){
		if(line == "") continue;
		cout << "\tadding " << line << endl;
		shows.push_back(line);
	}

	if(shows.size() == 0){
		cout << "Empty playlist" << endl;
		updateStatusFile(outputFile, "empty playlist", playlist, -1);
		return 2;
	}

	// play the playlist
	while(true){
		int index = 0;
		for(list<string>::iterator it = shows.begin(); it != shows.end(); it++){
			string show = *it;
			cout << "Playing " << show << endl;
			updateStatusFile(outputFile, "playing", playlist, index++);

			int pifmPID = fork();
			if(pifmPID == 0){
				string cmd = audioCMD + "\"" + baseDir + "shows/" + show + ".wav\"";
				system(cmd.c_str());
				exit(0);
			}
			
			int midiPID = fork();
			if(midiPID == 0){
				string cmd = midiCMD + "\"" + baseDir + "shows/" + show + ".mid\"";
				system(cmd.c_str());
				exit(0);
			}

			int status;
			waitpid(pifmPID, &status, 0);
			waitpid(midiPID, &status, 0);

			usleep(delay * 1000);
		}
	}

	return 0;
}

void updateStatusFile(string filename, string status, string playlist, int track){
	ofstream outputFile(filename.c_str());
	outputFile << status << endl << playlist << endl << track << endl;
	outputFile.close();
}
