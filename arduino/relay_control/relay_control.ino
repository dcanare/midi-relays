/*
int boardToPins[] = {
 9, 8, 7, 6, 5, 4, 3, 2,
 10, 11, 12, A0, A1, A2, A3, A4
};
*/
int boardToPins[] = {
  A4, A3, A2, A1, A0, 12, 11, 10,
  9, 8, 7, 6, 5, 4, 3, 2
};

void setup(){
  Serial.begin(115200);
  for(int i=0; i<16; i++){
    pinMode(boardToPins[i], OUTPUT);
    digitalWrite(boardToPins[i], HIGH);
  }
}

void loop(){
  if(Serial.available() > 0){
    byte input = Serial.read();
    digitalWrite(boardToPins[input >> 1], 1 - (input & 1));
  }
}
